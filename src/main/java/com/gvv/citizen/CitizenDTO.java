package com.gvv.citizen;

public class CitizenDTO {
    private String publicId;
    private String name;

    public CitizenDTO() {
    }

    public CitizenDTO(String publicId, String name) {
        this.setPublicId(publicId);
        this.setName(name);
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
