package com.gvv.citizen;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

@RestController
@RequestMapping("/citizen-api/v1")
public class CitizenController {

    private Set<CitizenDTO> citizenSet = new HashSet<>();

    public CitizenController(){
        citizenSet.add(new CitizenDTO("123", "German Valencia"));
        citizenSet.add(new CitizenDTO("456", "Ruth Vargas"));
        citizenSet.add(new CitizenDTO("789", "German Contreras"));
        citizenSet.add(new CitizenDTO("012", "Nicolas"));
        citizenSet.add(new CitizenDTO("345", "Camilo"));
        citizenSet.add(new CitizenDTO("678", "Adriana"));
        citizenSet.add(new CitizenDTO("901", "Milciades"));
    }

    @PostMapping("/citizen")
    public ResponseEntity<CitizenDTO> validateCitizen(@RequestBody @Valid CitizenDTO citizen) throws InterruptedException {

        int time = mockCallTime();
        Thread.sleep(time);

        return citizenSet.stream()
            .filter(citizenDTO ->
                 citizenDTO.getPublicId().equalsIgnoreCase(citizen.getPublicId())
                    && citizenDTO.getName().equalsIgnoreCase(citizen.getName())
            )
            .findAny()
            .map(match -> ResponseEntity.ok().body(new CitizenDTO(match.getPublicId(), match.getName())))
            .orElseGet(() -> ResponseEntity.noContent().build());
    }

    private static int mockCallTime() {
        Random ran = new Random();
        return (ran.nextInt(6) + 5)*1000;
    }
}
