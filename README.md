## Citizen API

This API simulates a citizens identification system. It is used to query people
who are likely to be added to the trusted contact list.

### Install the following tools

- jdk 1.8
- maven 3.5

### Getting Started

This is a spring boot application wrapped as maven module. So, after cloning the repository,
please go to the project root folder and execute the following commands:

- Generate the binary distribution

`$ mvn clean package`

- Run the application

```
$ cd target
$ java -jar citizen-api-0.0.1-SNAPSHOT.jar

```

The above will run the mock api in port [1986](http://localhost:1986)